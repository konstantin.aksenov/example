<?php
require_once __DIR__ . '/vendor/autoload.php';
new PhpAmqpLib\Connection\AMQPStreamConnection(
  env('RABBITMQ_HOST'),
  env('RABBITMQ_PORT'),
  env('RABBITMQ_USER'),
  env('RABBITMQ_PASSWORD'),
  env('RABBITMQ_VHOST')
);