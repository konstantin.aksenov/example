{{- define "backend_envs" }}
- name: APP_ENV
{{ if eq .Values.global.run_tests "no" }}
  value: "production"
{{ else }}
  value: "testing"
{{ end }}
- name: APP_KEY
  value: "{{ pluck .Values.global.env .Values.app.key | first | default .Values.app.key._default }}"
- name: APP_DEBUG
  value: "false"
- name: DB_CONNECTION
  value: "pgsql"
- name: DB_HOST
  value: "{{ .Chart.Name}}-postgresql"
- name: DB_PORT
  value: "5432"
- name: DB_DATABASE
  value: "{{ pluck .Values.global.env .Values.db.database | first | default .Values.db.database._default }}"
- name: DB_USERNAME
  value: "{{ pluck .Values.global.env .Values.db.username | first | default .Values.db.username._default }}"
- name: DB_PASSWORD
  value: "{{ pluck .Values.global.env .Values.db.password | first | default .Values.db.password._default }}"
- name: RABBITMQ_HOST
  value: "{{ pluck .Values.global.env .Values.infr.rmq.host | first | default .Values.infr.rmq.host._default }}"
- name: RABBITMQ_PORT
  value: "5672"
{{- $vhost := pluck .Values.global.env .Values.infr.rmq.vhosts | first | default .Values.infr.rmq.vhosts._default  }}
- name: RABBITMQ_VHOST
  value: "{{ $vhost.name }}"
- name: RABBITMQ_USER
  value: "{{ $vhost.user }}"
- name: RABBITMQ_PASSWORD
  value: "{{ $vhost.password }}"
- name: SENTRY_LARAVEL_DSN
  value: "{{ pluck .Values.global.env .Values.infr.sentry.dsn | first }}"
{{- end }}
