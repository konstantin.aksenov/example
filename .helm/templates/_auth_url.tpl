{{- define "auth_url" }}
{{- if ne .Values.global.env "production" }}
ingress.kubernetes.io/auth-url: "http://dev-auth.dev-auth-infra.svc.cluster.local/"
{{- end }}
{{- end }}
