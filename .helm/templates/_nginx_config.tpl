{{ define "nginx-config" -}}
worker_processes 1;
daemon off;

events {
    worker_connections 5000;
    multi_accept on;
    use epoll;
}

http {
    log_format json_combined escape=json '{ "time_local": "$time_local", '
     '"host": "$host", '
     '"remote_addr": "$remote_addr", '
     '"remote_user": "$remote_user", '
     '"request": "$request", '
     '"status": "$status", '
     '"body_bytes_sent": "$body_bytes_sent", '
     '"request_time": "$request_time", '
     '"http_referrer": "$http_referer", '
     '"http_user_agent": "$http_user_agent" }';

    server_tokens off;
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 65;
    types_hash_max_size 2048;
    client_max_body_size 100M;

    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    error_log /dev/stderr info;

    gzip on;
    gzip_disable "msie6";

    limit_upstream_zone limit 10m;

    upstream php-fpm {
        server 127.0.0.1:9000 max_fails=0;
        limit_upstream_conn limit=20 zone=limit backlog=100 timeout=10;
    }

    upstream php-fpm-ping {
        server 127.0.0.1:9000 max_fails=0;
    }

    server {
        charset utf-8;
        listen 80;
        server_name _;

        root        /app/public;
        index       index.php;

        set_real_ip_from  0.0.0.0/0;
        access_log       /dev/stdout json_combined;
        error_log        /dev/stderr info;

        location = /healthz {
            access_log  off;
            add_header Content-Type text/plain;
            return 200;
        }

        location = /ping {
            access_log  off;
            fastcgi_pass php-fpm-ping;
            include fastcgi_params;
            fastcgi_param SCRIPT_FILENAME $fastcgi_script_name;
        }

        location = /status {
            allow 10.0.0.0/8;
            allow 127.0.0.0/8;
            deny all;
            fastcgi_pass php-fpm-ping;
            include fastcgi_params;
            fastcgi_param SCRIPT_FILENAME $fastcgi_script_name;
            access_log  off;
        }

        location /backend/questions/ {
            index index.html;
            alias /app/public/spec/;
        }

        location / {
            try_files $uri $uri/ /index.php?$args;
        }

        location ~ \.php$ {
            try_files $uri /index.php =404;
            fastcgi_pass php-fpm;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include fastcgi_params;
        }

        location ~ /\.(ht|svn|git) {
            deny all;
        }
    }
}
{{ end }}
